package com.provider.oauth;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Setup {
	@Autowired
	UserRepository em;
	@PostConstruct
	public void initializeDB() {
		User user = new User();
		user.setClientId("admin");
		user.setClientSecret("admin");
		user.setClientName("admin");
		user.setActive(true);
		
		em.save(user);
		
	}

}
